function goToAdd() {
  window.location.replace('./add.php');
}

function cancel() {
  window.location.replace('./index.php');
}

$(".topButton").mouseover(function() {
    $("#" + this.id).css({"backgroundColor": "black", "color": "white", "boxShadow": "4px 4px white", "border": "solid rgb(0,0,0) 3px", "cursor": "pointer"});
  });

$(".topButton").mouseout(function() {
      $("#" + this.id).css({"backgroundColor": "rgb(240, 240, 240)", "color": "black", "boxShadow": "4px 4px black", "border": "solid rgb(0,0,0) 3px"});
  });
