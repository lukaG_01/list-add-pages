<?php
include('retrieve_item.php');
include('item.php');

$skuArray = [];

foreach ($array as $item) {
  $tmp = unserialize($item);
  array_push($skuArray, $tmp->get_sku());
}

$isUnique = "unique";
if (in_array($_POST["SKU"], $skuArray)) {
  $isUnique = "";
}

echo $isUnique;

?>
