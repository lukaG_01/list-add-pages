<?php
include('item.php');

if (isset($_POST['sku'])) {
$skuInput = $_POST['sku'];
$nameInput = $_POST['name'];
$priceInput = $_POST['price'];
$attrInput = $_POST['attr'];

$item = new Item($skuInput, $nameInput, $priceInput, $attrInput);

$sql = "SELECT itemsJSON FROM items";
$result = $conn->query($sql);
$row = mysqli_fetch_array($result);

$newItem = serialize($item);

$updatedJSON = strlen($row['itemsJSON']) != 0 ? $row['itemsJSON'] . ' | ' .serialize($item)  :  serialize($item);

$sql1 = "UPDATE items SET itemsJSON = '$updatedJSON'";

if (!$conn->query($sql1)) {
  echo mysqli_error($conn);
}

}
?>
