<?php

class Item {

    public $sku;
    public $name;
    public $price;
    public $attr;

    public function __construct($sku, $name, $price, $attr) {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->attr = $attr;
    }

    public function print(){
        echo $this->sku . "\n<br>" . $this->name . "\n<br>" . $this->price . "\n<br>" . $this->attr . "\n<br>";
    }

    public function get_sku() {
      return $this->sku;
    }
}

?>
