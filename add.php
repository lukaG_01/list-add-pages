<?php
include('connection.php');
 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="addStyling.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>

      <div id="addPage">
        <div id="topPart">
          <div id="productAddHeader">
            Product Add
          </div>
          <div class="topButton" id="saveButton" onclick="save()">
            Save
          </div>
          <div class="topButton" id="cancelButton" onclick="cancel()">
            Cancel
          </div>
        </div>

        <div id="borderlineTop"></div>
        <div id="borderlineBottom"></div>

        <div id="inputPart">

          <form action="/action_page.php">
            <table style="width:100%">
              <tr>
                <th>SKU</th>
                <td><input type="text" id="SKUinput" name="SKUinput"><br></td>
              </tr>
              <tr>
                <th>Name</th>
                <td><input type="text" id="NameInput" name="NameInput"><br></td>
              </tr>
              <tr>
                <th>Price ($)</th>
                <td><input type="number" id="PriceInput" name="PriceInput"><br></td>
              </tr>

            </table>
          </form>

<br>

          <label for="typeSwitcher" id="typeSwitcherLabel">Type Switcher</label>

            <select name="typeSwitcher" id="typeSwitcher">
              <option value="" selected disabled hidden>Type Switcher</option>
              <option value="dvd">DVD</option>
              <option value="book">Book</option>
              <option value="furniture">Furniture</option>
            </select>

<br><br><br>

              <div id="dvdDescription" class="description">
                <table style="width:100%">
                  <tr>
                    <th>Size (MB)</th>
                    <td><input type="number" id="MBinput" name="MBinput"><br></td>
                  </tr>
                </table>
                <div>
                  Please provide storage capacity in Megabytes.
                </div>
              </div>

              <div id="bookDescription" class="description">
                <table style="width:100%">
                  <tr>
                    <th>Weight (KG)</th>
                    <td><input type="number" id="KGinput" name="KGinput"><br></td>
                  </tr>
                </table>
                <div>
                  Please provide the weight of the item in Kilograms.
                </div>
              </div>

              <div id="furnitureDescription" class="description">
                <table style="width:100%">
                  <tr>
                    <th>Height (CM)</th>
                    <td><input type="number" id="heightInput" name="heightInput"><br></td>
                  </tr>
                  <tr>
                    <th>Width (CM)</th>
                    <td><input type="number" id="widthInput" name="widthInput"><br></td>
                  </tr>
                  <tr>
                    <th>Length (CM)</th>
                    <td><input type="number" id="lengthInput" name="lengthInput"><br></td>
                  </tr>

                </table>
                <br>
                <div>
                  Please provide dimensions in HxWxL format.
                </div>
              </div>

        </div>
        <div id="errorBox">

        </div>
      </div>

        <footer id="bottomPart">
          <div id="borderlineTop"></div>
          <div id="borderlineBottom"></div>
            Scandiweb Test assignment
        </footer>


        <form action="index.php" method="post">
              <input type="hidden" name="sku" id="sku">
              <input type="hidden" name="name" id="name">
              <input type="hidden" name="price" id="price">
              <input type="hidden" name="attr" id="attr">
              <input  class="btn" type="submit" name="addItem" id="addItem" style="display:none;">
        </form>

    <script type="text/javascript">
    function save() {
          $("#sku").val($("#SKUinput").val());
          $("#name").val($("#NameInput").val());
          $("#price").val($("#PriceInput").val());

          let selectedType = $("select#typeSwitcher").children("option:selected").val();

          if (selectedType == "dvd") {
            $("#attr").val($("#MBinput").val());
          } else if (selectedType == "book") {
            $("#attr").val($("#KGinput").val());
          } else if (selectedType == "furniture") {
            $("#attr").val($("#heightInput").val() + "x" + $("#widthInput").val() + "x" + $("#lengthInput").val());
          }

          let errors = isValid($("#sku").val(), $("#name").val(), $("#price").val(), $("#attr").val());

          $.ajax({
                 url: 'isUnique.php',
                 type: "POST",
                 data: {'SKU' : $("#sku").val() },
                 success: function(resp){
                               if (resp == "unique") {
                                  if (errors == "") {
                                      $("#errorBox").css({"visibility": "hidden"});
                                      $("#addItem").click();
                                  } else {
                                      $("#errorBox").css({"visibility": "visible"});
                                      $("#errorBox").html(errors);
                                  }
                               } else {
                                 errors += "Please, choose another SKU. That one is already assigned to another item\n";
                                 $("#errorBox").css({"visibility": "visible"});
                                 $("#errorBox").html(errors);
                               }
                          }
             });
      }
    </script>
<script src="buttons.js"> </script>
<script src="typeSwitcher.js"> </script>
<script src="validate.js"> </script>

    </body>
</html>
