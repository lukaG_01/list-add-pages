function isValid(sku, name, price, attribute) {
    let errors = "";

    if (typeIsChosen() == false) {
      errors += "Please, choose the type of the item, and fill out the required fields<br>";
    }

    if (isEmpty(sku) || isEmpty(name) || isEmpty(price) || isEmpty(attribute)) {
      errors += "Please, submit required data<br>";
    }

    return errors;
}


function isEmpty(input) {
    return !input.trim().length;
}

function typeIsChosen() {
    return $("#typeSwitcher").val() != null ? true : false;
}
