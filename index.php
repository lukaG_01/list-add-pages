<?php
include('connection.php');
include('help_add_item.php');
include('retrieve_item.php');
include('delete_items');
 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="styling.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    </head>
    <body>

      <div class="listPage">
        <div id="topPart">
          <div id="productListHeader">
            Product List
          </div>
          <div class="topButton" id="addButton" onclick="goToAdd()">
            ADD
          </div>
          <div class="topButton" id="deleteButton" onclick="deleteItems()">
            MASS DELETE
          </div>
        </div>

        <div id="borderlineTop"></div>
        <div id="borderlineBottom"></div>

        <div id="listPart">

          <?php
            foreach ($array as $item) {
              $tmp = unserialize($item);
              ?>
              <div class="item"><input type="checkbox" name="itemCheckbox"><?php echo $tmp->print(); ?></div>
              <?php } ?>
        </div>
      </div>

        <footer id="bottomPart">
          <div id="borderlineTop"></div>
          <div id="borderlineBottom"></div>
            Scandiweb Test assignment
        </footer>

<script>
function deleteItems() {
  let checkedArray = new Array();
  let newArray = `<?php echo serialize($array) ?>`;


$("input:checkbox[name=itemCheckbox]:checked").each(function(){
    let itemDescription = ($(this).parent().text()).split("\n");

    $.ajax({
                async: false,
                url: 'delete_items.php',
                type: "POST",
                data: {'delete_sku': itemDescription[0],
                       'delete_name': itemDescription[1],
                       'delete_price': itemDescription[2],
                       'delete_attr': itemDescription[3],
                       'arr' : newArray },
                success: function(response) {
                  newArray = response;
                }
            });
  });

  $.ajax({
              url: 'updateDeletion.php',
              type: "POST",
              data: {'newArray': newArray },
              success: function(response) {
                window.location.replace('./index.php');
              }
          });
}

</script>

<script src="buttons.js"></script>

    </body>
</html>
